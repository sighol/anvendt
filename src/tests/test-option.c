#include "minunit.h"
#include <stdio.h>

#include "util/optparse.h"

///private
///endprivate

int tests_run = 0;

static char* argv[6] = {"test-option", "arg1", "--opt1", "-2", "arg2", "-3"};
static int argc = 6;

static char* test_option_init_destroy() {
	OptionParser* parser = option_new(argc, argv);
	option_destroy(parser);
	return 0;
}

static char* test_option_sizes() {
	OptionParser* parser = option_new(argc, argv);
	mu_assert("option size", parser->option_size == 3);
	mu_assert("argument size", parser->argument_size == 2);
	option_print(parser);

	mu_assert("has option: opt1", option_has_option(parser, "opt1") == true);
	mu_assert("has not option: opt100", option_has_option(parser, "opt100") == false);

	return 0;
}

static char * all_tests() {
	mu_run_test(test_option_init_destroy);
	mu_run_test(test_option_sizes);
	return 0;
}

int main(int argc, char **argv) {
	char *result = all_tests();
	if (result != 0) {
		printf("FAIL: %s\n", result);
	} else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);

	return result != 0;
}