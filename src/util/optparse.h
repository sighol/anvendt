#pragma once

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
	char** options;
	char** arguments;
	int option_size;
	int argument_size;
} OptionParser;

///public
OptionParser* option_new(int argc, char** argv);
void option_destroy(OptionParser* self);
bool option_has_option(OptionParser* self, char* search);
void option_print(OptionParser* self);
///endpublic
