#include "optparse.h"
#include <stdio.h>


///private
void option_add(OptionParser* self, char* arg);
bool strcmp2(const char* first, const char* second);
char* substr(char* input, int from);
///endprivate

///efun
OptionParser* option_new(int argc, char** argv) {
	OptionParser* self = calloc(1, sizeof(OptionParser));
	self->options = (char**) calloc(argc, sizeof(char*));
	self->arguments = (char**) calloc(argc, sizeof(char*));
	for (int i = 1; i < argc; i++) {
		option_add(self, argv[i]);
	}
	return self;
}

///efun
void option_destroy(OptionParser* self) {
	free(self);
}

///pfun
void option_add(OptionParser* self, char* arg) {
	int len = strlen(arg);
	// The length is less than 2, it can't be an option
	if (len < 2) {
		return;
	}
	if (arg[0] == '-' && arg[1] == '-') {
		self->options[self->option_size] = substr(arg, 2);
		self->option_size++;
	} else if (arg[0] == '-') {
		self->options[self->option_size] = substr(arg, 1);
		self->option_size++;
	} else{
		self->arguments[self->argument_size] = arg;
		self->argument_size++;
	}
}

///efun
bool option_has_option(OptionParser* self, char* search) {
	for (int i = 0; i < self->option_size; i++) {
		bool is_equal = strcmp2(search, self->options[i]);
		if (is_equal == true) {
			return true;
		}
	}
	return false;
}

///pfun
bool strcmp2(const char* first, const char* second) {
	int len1 = strlen(first);
	int len2 = strlen(second);
	if (len1 != len2) {
		return false;
	}

	for (int i = 0; i < len1; i++) {
		if (first[i] != second[i]) {
			return false;
		}
	}
	return true;
}

///pfun
char* substr(char* input, int from) {
	int len = strlen(input);
	char* sub = (char*)calloc(len-1, sizeof(char));
	memcpy(sub, &input[from], len - from);
	sub[len] = '\0';
	return sub;
}

///efun
void option_print(OptionParser* self) {
	for (int i = 0; i < self->option_size; i++) {
		printf("opt: %s\n", self->options[i]);
	}
	for(int i = 0; i < self->argument_size; i++) {
		printf("arg: %s\n", self->arguments[i]);
	}
}
