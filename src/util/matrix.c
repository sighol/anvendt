#include "matrix.h"

#define get(self, r, c) self->cols * r + c

///private
///endprivate

///efun
Matrix* matrix_new(int rows, int cols) {
	Matrix* self = calloc(1, sizeof(Matrix));
	self->rows = rows;
	self->cols = cols;
	self->matrix = (double*) calloc(rows * cols, sizeof(double));
	return self;
}

///efun
void matrix_destroy(Matrix* self) {
	free(self->matrix);
	free(self);
}

///efun
void matrix_input(Matrix* self, double* matrix) {
	for (int row = 0; row < self->rows; row++) {
		for (int col = 0; col < self->cols; col++) {
			int x = get(self, row, col);
			self->matrix[x] = matrix[x];
		}
	}
}

///efun
Matrix* matrix_new_input(int rows, int cols, double* matrix) {
	Matrix* self = matrix_new(rows, cols);
	matrix_input(self, matrix);
	return self;
}

///efun
void matrix_print(Matrix* self) {
	for (int row = 0; row < self->rows; row++) {
		for (int col = 0; col < self->cols; col++) {
			int x = get(self, row, col);
			printf("%7.3f ", self->matrix[x]);
		}
		printf("\n");
	}
	printf("\n");
}

///efun
Matrix* matrix_multiply(Matrix* self, Matrix* other) {
	Matrix* res = matrix_new(self->rows, other->cols);
	for (int i = 0; i < self->rows; i++) {
		for (int j = 0; j < other->cols; j++) {
			for (int k = 0; k < self->cols; k++) {
				res->matrix[get(res, i, j)] += self->matrix[get(self, i, k)] * other->matrix[get(other, k, j)];
			}
		}
	}
	return res;
}

///efun
Matrix* matrix_add(Matrix* self, Matrix* other) {
	Matrix* res = matrix_new(self->rows, self->cols);
	for (int i = 0; i < self->rows; i++) {
		for (int j = 0; j < self->cols; j++) {
			int x = get(self, i, j);
			res->matrix[x] = self->matrix[x] + other->matrix[x];
		}
	}
	return res;
}

///efun
Matrix* matrix_sub(Matrix* self, Matrix* other) {
	Matrix* res = matrix_new(self->rows, self->cols);
	for (int i = 0; i < self->rows; i++) {
		for (int j = 0; j < self->cols; j++) {
			int x = get(self, i, j);
			res->matrix[x] = self->matrix[x] - other->matrix[x];
		}
	}
	return res;
}

///efun
Matrix* matrix_multiply_scalar(Matrix* self, double scalar) {
	Matrix* res = matrix_new(self->rows, self->cols);
	for (int i = 0; i < self->rows; i++) {
		for (int j = 0; j < self->cols; j++) {
			int x = get(self, i, j);
			res->matrix[x] = self->matrix[x] * scalar;
		}
	}
	return res;
}

///efun
double matrix_get(Matrix* self, int row, int col) {
	int index = get(self, row, col);
	return self->matrix[index];
}