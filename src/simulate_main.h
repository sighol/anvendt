#include "graphics/graphics.h"
#include "graphics/view.h"
#include "graphics/graph.h"
#include "controller/derivator.h"
#include "graphics/glututil.h"
#include "util/matrix.h"
#include "GL/freeglut.h"


#include <stdbool.h>

typedef struct {
	Graph* graph;

	Derivator* derivator;
	int elapsed_time;

	double setpoint;

	double next;
	int width, height;
	View* view;

	double power_p, power_i, power_d, power;
} Main;


///public
int main(int argc, char* argv[]);
Main* main_new(void);
void keyboard_callback(unsigned char key, int x, int y);
void reshape_callback(int width, int height);
void idle_callback(void);
void display_callback(void);
void simulate_controller(Main* self);
void simulate_apply_power(Main* self, double* pos, double* vel, double power);
void close_callback(void);
///endpublic

