#include "view_history.h"

///private
///endprivate

Main* main_global;

///efun
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);

	OptionParser* parser = option_new(argc, argv);
	if (parser->argument_size < 1) {
		printf("You have not given an input file\n");
		exit(EXIT_FAILURE);
	}

	Main* self = main_new();
	main_global = self;
	glutIdleFunc(idle_callback);
	glutDisplayFunc(display_callback);
	glutReshapeFunc(reshape_callback);
	glutKeyboardFunc(keyboard_callback);

	if (option_has_option(parser, "f")) {
		glutFullScreen();
	}

	view_read_from_file(self->view, parser->arguments[0]);
	glutMainLoop();
};

///efun
Main* main_new(void) {
	Main* self = (Main*) malloc(sizeof(Main));
	self->width = 800;
	self->height = 600;
	graphics_init(self->width, self->height);
	self->last = 0;
	self->next = 0;
	self->view = view_new();
	self->graph = self->view->velocity_graph;
	glClearColor(0.6, 0.6, 0.6, 1);

	return self;
}

///efun
void idle_callback(void) {
	glutPostRedisplay();
}

///efun
void keyboard_callback(unsigned char key, int x, int y) {
	Main* self = main_global;
    if (key == 27) {
        exit(0);
    }

    if (key == GLUT_KEY_F) {
    	toggle_fullscreen(self->width, self->height);
    }
}

///efun
void reshape_callback(int width, int height) {
	Main* self = main_global;
	view_reshape(self->view, width, height);
    glViewport(0, 0, width, height);
    glScissor(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, /*near=*/0, /*far=*/1);

    glutPostRedisplay();
}

///efun
void display_callback(void) {
	Main* self = main_global;

	view_draw(self->view);

	glutSwapBuffers();
	glFlush();
}
