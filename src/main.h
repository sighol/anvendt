#pragma once

#include <GL/glew.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <GL/freeglut.h>
#include <time.h>


#include "controller/controller.h"
#include "graphics/graphics.h"
#include "graphics/graph.h"
#include "graphics/history.h"
#include "graphics/view.h"
#include "graphics/glututil.h"


typedef struct {
	Controller* controller;
	View* view;

	int elapsed_time;
} Main;


///public
int main(int argc, char* argv[]);
Main* main_new(void);
void main_destroy(Main* self);
void main_display_callback(void);
void main_run_controller(Main* self);
void main_push_graph_data(Main* self);
void main_reshape_callback (int width, int height);
void keyboard_callback(unsigned char key, int x, int y);
void main_history_clean(Main* self);
void main_idle_callback(void);
void main_close_callback(void);
void main_write_to_file(Main* self);
void main_exit(Main* self);
///endpublic
