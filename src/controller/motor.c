#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "util/util.h"


#include "motor.h"


///private
void motor_init_default(Motor* self);
void motor_open(Motor* self);
void motor_wait_or_die(Motor* self);
///endprivate


///efun
Motor* motor_new(void) {
	Motor* self = (Motor*) calloc(1, sizeof(Motor));
	motor_init_default(self);
	motor_open(self);
	motor_wait_or_die(self);
	return self;
}

///pfun
void motor_init_default(Motor* self) {
	self->servo = 0;
}

///pfun
void motor_open(Motor* self) {
	CPhidgetServo_create(&self->servo);
	// Establishes contact with the motor. The second argument is the serial
	// number of the component, -1 means any.
	CPhidget_open((CPhidgetHandle) self->servo, -1);
}

///pfun
void motor_wait_or_die(Motor* self) {
	// Exits the program if the device isn't connected.
	int result;
	printf("Waiting for Servo controller to be attached.... ");
	fflush(stdout);
	if((result = CPhidget_waitForAttachment((CPhidgetHandle) self->servo, 3000))) {
		const char *err;
		CPhidget_getErrorDescription(result, &err);
		printf("ERROR\nMotor: Problem waiting for attachment: %s\n", err);
		exit(EXIT_FAILURE);
	}
	printf("OK\n");
	return;
}

///efun
void motor_start(Motor* self) {
	// Starts the motor.
	CPhidgetServo_setEngaged (self->servo, 0, 0);
	// Sets motor power to 0, no speed.
	motor_set_power(self, 0);
}

///efun
void motor_destroy(Motor* self) {
	CPhidget_close((CPhidgetHandle) self->servo);
	CPhidget_delete((CPhidgetHandle) self->servo);
}

///efun
void motor_set_power(Motor* self, double power) {
	// The motor goes forward between 80 and 160, where 160 is max power
	// forward. The motor goes max backward at 40 and stands still at 80. The
	// input power is saturized between -1 and 1

	// Demands that the input power is between -1 and 1
	if (power > 1.0) {
		power = 1.0;
	} else if (power < -1.0) {
		power = -1.0;
	}

	// Converts between [-1, 1] to [40, 160]
	double motor_value = 0;
	if (power > 0) {
		motor_value = normalize(power, -1, 1, 0, 160);
	} else {
		motor_value = normalize(power, -1, 1, 40, 120);
	}
	CPhidgetServo_setPosition (self->servo, 0, motor_value);
}
