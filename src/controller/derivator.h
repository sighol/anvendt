#pragma once

typedef struct {
	double data;
	double time;
} HistElement;

typedef struct {
	HistElement* history;
	int max_size;
	int size;
} Derivator;

///public
Derivator* derivator_new(int max_size);
void derivator_destroy(Derivator* self);
void derivator_push(Derivator* self, int value);
double derivator_speed(Derivator* self);
void derivator_print(Derivator* self);
///endpublic
