#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "sensor.h"

///private
void sensor_init_default(Sensor* self);
void sensor_open(Sensor* self);
int sensor_change_handler(CPhidgetInterfaceKitHandle ifKit, void *sensor, int index, int value);
void sensor_set_distance(Sensor* self, int value);
void sensor_wait_or_die(Sensor* self);
void sensor_set_change_trigger(Sensor* self, int value);
///endprivate

///efun
Sensor* sensor_new(void) {
	Sensor *self = (Sensor*) calloc(1, sizeof(Sensor));
	sensor_init_default(self);
	sensor_open(self);
	sensor_wait_or_die(self);
	sensor_set_change_trigger(self, 1); // To make the sensor more sensitive
	return self;
}

///pfun
void sensor_init_default(Sensor* self) {
	self->ifKit = 0;
	self->last_value = -1;
	self->distance = 0;
}

///efun
void sensor_destroy(Sensor* self) {
	CPhidget_close((CPhidgetHandle) self->ifKit);
	CPhidget_delete((CPhidgetHandle) self->ifKit);
	free(self);
}

///pfun
void sensor_open(Sensor* self) {
	CPhidgetInterfaceKit_create(&self->ifKit);

	// This function is run every time the sensor value has changed the amount
	// specified by the sensorChangeTrigger.
	CPhidgetInterfaceKit_set_OnSensorChange_Handler(self->ifKit, sensor_change_handler, self);

	// Establishes contact with the sensor. The second argument is the serial
	// number of the component, -1 means any.
	CPhidget_open((CPhidgetHandle)self->ifKit, -1);
}

///pfun
int sensor_change_handler(CPhidgetInterfaceKitHandle ifKit, void *sensor, int index, int value) {
	Sensor* self = (Sensor*) sensor;

	// The interfacekit can handle 8 sensors at once. The angle sensor is
	// inserted into port 0. If the sensor isn't the angle sensor, it's of no
	// interest.
	if (index != 0) {
		return 0;
	}

	// If self->last_value == -1, then this is the first time this function has
	// been called. Then the first time this function is run, last_value is set
	// to the initial value of the sensor.
	if (self->last_value == -1) {
		self->last_value = value;
	}
	sensor_set_distance(self, value);
	return 0;
}

///pfun
void sensor_set_distance(Sensor* self, int value) {
	// Difference if sensor doesn't jump
	int diff = value - self->last_value;
	// Difference if sensor jumps over 1000
	int diff_inv = 1000 - diff;
	if (diff < -500) {
		// The sensor has jumped from below 1000 to above 0 => Distance increase.
		self->distance += diff_inv;
	} else if (diff > 500) {
		// The sensor has jumped from above 0 to below 1000 => Distance decrease.
		self->distance -= diff_inv;
	} else {
		self->distance += diff;
	}
	self->last_value = value;
}

///pfun
void sensor_wait_or_die(Sensor* self) {
	// Exits the program if the device isn't attached
	const char *err;
	int result;
	printf("Waiting for interface kit to be attached.... ");
	fflush(stdout);
	if((result = CPhidget_waitForAttachment((CPhidgetHandle) self->ifKit, 3000))) {
		CPhidget_getErrorDescription(result, &err);
		printf("ERROR\nSensor: Problem waiting for attachment: %s\n", err);
		exit(EXIT_FAILURE);
	}
	printf("OK\n");
}

///pfun
void sensor_set_change_trigger(Sensor* self, int value) {
	// Specifies the amount the sensor value has to change to trigger the sensor
	// change handler.
	int numSensors;
	CPhidgetInterfaceKit_getSensorCount(self->ifKit, &numSensors);
	for (int i = 0; i < numSensors; i++) {
		CPhidgetInterfaceKit_setSensorChangeTrigger(self->ifKit, i, value);
	}
}
