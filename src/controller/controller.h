#pragma once

#include "controller/sensor.h"
#include "controller/motor.h"
#include "controller/derivator.h"


typedef struct {
	int setpoint;
	double integrator;
	double Kp;
	double Ki;
	double Kd;

	Sensor* sensor;
	Motor* motor;
	Derivator* derivator;

	double power; //Normalized bewteen -1 and 1
	double speed;
	double diff;

	double power_p, power_d, power_i;
} Controller;

///public
Controller* controller_new(int setpoint);
void controller_destroy(Controller* self);
void controller_begin(Controller *self);
void controller_add_pid(Controller* self);
void controller_add_lqr(Controller* self);
void controller_end(Controller* self);
///endpublic
