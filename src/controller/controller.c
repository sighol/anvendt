#include "controller.h"

#include "util/matrix.h"
#include <stdio.h>
#include <stdlib.h>


///private
void controller_init_default(Controller* self);
void controller_accumulate_integrator(Controller* self);
void controller_reduce_integrator_if_too_big(Controller* self);
///endprivate

///efun
Controller* controller_new(int setpoint) {
	Controller* self = (Controller*) calloc(1, sizeof(Controller));
	controller_init_default(self);

	self->setpoint = setpoint;

	self->sensor = sensor_new();
	self->motor = motor_new();
	self->derivator = derivator_new(20);

	motor_start(self->motor);
	return self;
}

///pfun
void controller_init_default(Controller* self) {
	self->Kp = 2.5e-3;
	self->Ki = 2.52e-6;
	self->Kd = 1.5;

	self->integrator = 0;

	self->setpoint = 0;

	self->power = 0;
	self->speed = 0;
	self->diff = 0;
}

///efun
void controller_destroy(Controller* self) {
	motor_set_power(self->motor, 0);
	motor_destroy(self->motor);
	sensor_destroy(self->sensor);
	derivator_destroy(self->derivator);

	free(self);
}

///efun
void controller_begin(Controller *self) {
	// Should be called at the beginning of an iteration.
	self->power = 0;
	derivator_push(self->derivator, self->sensor->distance);
	self->speed = derivator_speed(self->derivator);
	self->diff = self->setpoint - self->sensor->distance;
	controller_accumulate_integrator(self);
	controller_reduce_integrator_if_too_big(self);
}

///pfun
void controller_accumulate_integrator(Controller* self) {
	double diff = self->diff;

	// Sets max_diff to what the diff would be to make the proportional power
	// equals to 1. This was set as a starting point, but turned out to work
	// fine.
	double diff_max = 1/self->Kp;
	if (diff > diff_max) {
		self->integrator += diff_max;
	} else if (diff < -diff_max) {
		self->integrator -= diff_max;
	} else if(abs(diff) > 20) {
		double start = 0.5;
		double start_diff = start * diff_max * (diff > 0 ? 1 : -1);
		diff = start_diff + (1-start)*diff;
		self->integrator += diff;
	} else {
		self->integrator += diff;
	}
}

///pfun
void controller_reduce_integrator_if_too_big(Controller* self) {
	// As 1.0  is the biggest possible value for the power, it doesn't make
	// sense to allow the integrator to be so big that the integrator power is
	// bigger than 1.0
	double integrator_max = 1.0/(self->Ki);
	if (self->integrator > integrator_max) {
		self->integrator = integrator_max;
	} else if (self->integrator < - integrator_max) {
		self->integrator = - integrator_max;
	}
}

///efun
void controller_add_pid(Controller* self) {
	double power_p = self->Kp * self->diff;
	double power_i = self->Ki * self->integrator;
	double power_d = self->Kd * self->speed;

	double power = power_p + power_i - power_d;

	if (power < 0) {
		power = 0;
	}
	self->power += power;

	// To make other modules able to read these values.
	self->power_p = power_p;
	self->power_i = power_i;
	self->power_d = power_d;
}

///efun
void controller_add_lqr(Controller* self) {
	// Values found from matlab simulations.
	double k_data[] = {
	     5.0554e-03,
	     2.0732e+00,
	    -1.2247e-06,
	};
	double x_data[] = {-self->diff, self->speed, self->integrator};
	Matrix* K = matrix_new_input(1, 3, k_data);
	Matrix* x = matrix_new_input(3, 1, x_data);

	Matrix* Kx = matrix_multiply(K, x);


	double u = - matrix_get(Kx, 0, 0);
	if (u < 0) {;
		u = 0;
	}

	self->power += u;
	matrix_destroy(K);
	matrix_destroy(x);
	matrix_destroy(Kx);
}

///efun
void controller_end(Controller* self) {
	// Applies the power to the motor.
	motor_set_power(self->motor, self->power);
}

