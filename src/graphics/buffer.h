#pragma once

#include <GL/glew.h>


typedef struct {
	GLuint buffer_id;
	GLuint vao_id;
} Buffer;

typedef struct {
	GLubyte r, g, b, a;
} Color;

typedef struct {
	GLfloat x, y, z;
} Position;

typedef struct {
	Color color;
	Position position;
} VertexData;

///public
Buffer* buffer_new(void);
void buffer_destroy(Buffer* self);
void buffer_bind(Buffer* self);
void buffer_print_vertex_data(size_t size, VertexData* data);
///endpublic
