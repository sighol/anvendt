#pragma once

#include <GL/glew.h>
#include "graphics/buffer.h"
#include "text.h"
#include "history.h"

typedef struct {
	Buffer* graph_buffer;
	Buffer* axis_buffer;
	Buffer* extra_buffer;

	double graph_max;
	double graph_min;
	GLuint color_shader_id;
	GLuint position_shader_id;
	double* axis_steps;
	int axis_steps_size;

	Color graph_color;

	TextWriter* text;
	History* history;
} Graph;


///public
Graph* graph_new(int history_size);
void graph_destroy(Graph* self);
void graph_push_data(Graph* self, double value);
void graph_set_line(Graph* self, double y);
void graph_draw(Graph* self);
void graph_draw_text(Graph* self, int width, int height);
double graph_last_value(Graph* self);
void graph_clean_history(Graph* self);
///endpublic
