#include <stdbool.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>

#include "glututil.h"

///private
///endprivate


///efun
void toggle_fullscreen(int width, int height) {
	static bool fullscreen = false;
	fullscreen = !fullscreen;
	if (fullscreen) {
		glutFullScreen();
	} else {
		glutReshapeWindow(width, height);
	}
}

///efun
void print_fps(void) {
	static int frames = 0;
	static int total_frames = 0;
	static int elapsed_time = 0;
	frames++;
	total_frames++;

	int time = glutGet(GLUT_ELAPSED_TIME);
	int time_diff = time - elapsed_time;
	if (time_diff > 1000) {
		char title_text[50];
		sprintf(title_text, "FPS: %d - Total: %d", frames, total_frames);
		elapsed_time = time;
		frames = 0;
		glutSetWindowTitle(title_text);
	}
}
