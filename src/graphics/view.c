#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>
#include <stdlib.h>

#include "graphics/shaderprogram.h"

#include "view.h"

///private
void view_reset_viewport_scissor(View* self);
void view_draw_graphs(View* self);
char* view_get_time_string(View* self);
void view_draw_graph(View* self, Graph* graph, int x, int y, char* name);
void view_draw_header(View* self, char* str, int x, int y);
///endprivate


///efun
View* view_new(void) {
	View* self = (View*) malloc(sizeof(View));
	int history_size = 5000;
	self->velocity_graph = graph_new(history_size);
	self->velocity_graph->graph_color = (Color){0, 160, 40, 255};
	self->power_graph = graph_new(history_size);
	self->power_graph->graph_color = (Color){25, 100, 205, 255};
	self->distance_graph = graph_new(history_size);
	self->boat = boat_new();
	self->text = text_new(GLUT_BITMAP_9_BY_15);
	TextColor c = {0, 0, 0};
	self->text->color = c;

	self->fields = (ViewField**) calloc(30, sizeof(ViewField*));
	self->field_size = 0;

	return self;
}

///efun
void view_destroy(View* self) {
	graph_destroy(self->velocity_graph);
	graph_destroy(self->power_graph);
	graph_destroy(self->distance_graph);
	text_destroy(self->text);
	free(self->fields);
	free(self);
}

///efun
void view_reshape(View* self, int width, int height) {
	self->width = width;
	self->height = height;
	double width_margin = self->width/20;
	double height_margin = self->height/20;

	self->plot_width = (self->width - 3*width_margin)/2;
	self->plot_height = (self->height - 3*height_margin)/2;
	self->x1 = width_margin;
	self->y1 = height_margin;
	self->x2 = width_margin*2 + self->plot_width;
	self->y2 = height_margin*2 + self->plot_height;
}

///efun
void view_clean_history(View* self) {
	// Removes all history from all graphs
	graph_clean_history(self->velocity_graph);
	graph_clean_history(self->power_graph);
	graph_clean_history(self->distance_graph);
}

///efun
void view_draw(View* self) {
	// Draws everything on the screen
	glClearColor(0.8, 0.8, 0.8, 1);
	view_reset_viewport_scissor(self);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	view_draw_graphs(self);
	view_draw_fields(self);
	graph_set_line(self->distance_graph, self->setpoint);
}

///pfun
void view_reset_viewport_scissor(View* self) {
	// Resets the viewport to the whole screen
	glScissor(0, 0, self->width, self->height);
	glViewport(0, 0, self->width, self->height);
}

///pfun
void view_draw_graphs(View* self) {
	glClearColor(0.5, 0.5, 0.5, 1);

	view_draw_graph(self, self->power_graph, self->x1, self->y1, "Power");
	view_draw_graph(self, self->distance_graph, self->x1 , self->y2, "Position");
	// view_draw_graph(self, self->velocity_graph, self->x2, self->y1, "Velocity");

	// Drawing the boat
	glScissor(self->x2, self->y2, self->plot_width, self->plot_height);
	glViewport(self->x2, self->y2, self->plot_width, self->plot_height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	boat_draw(self->boat, self->setpoint, self->position);

	// On some linux graphics drivers, the text isn't drawed if a shader program
	// is present. Therefor the shader program is disabled before text writing.
	shaderprogram_use_none();
	boat_draw_text(self->boat, self->width, self->height);
	view_reset_viewport_scissor(self);
	view_draw_header(self, "Figure", self->x2, self->y2);
}


///pfun
char* view_get_time_string(View* self) {
	static char time[60];
	int ms_time = glutGet(GLUT_ELAPSED_TIME);
	int hours = ms_time / (3600 * 1000);
	ms_time -= hours * 3600*1000;
	int mins = ms_time / (60*1000);
	ms_time -= mins * 60 * 1000;
	int seconds = ms_time / 1000;
	ms_time -= seconds * 1000;

	sprintf(time, "time: %02d:%02d:%03d", mins, seconds, ms_time);
	return time;
}
///pfun
void view_draw_graph(View* self, Graph* graph, int x, int y, char* name) {
	int width = self->plot_width;
	int height = self->plot_height;

	// resets viewport to be able to draw the header outside of the graph.
	view_reset_viewport_scissor(self);

	// Drawing graph header text
	if (graph->history->size > 10) {
		char str[40];
		double amp = graph_last_value(graph);
		sprintf(str, "%s: %10.4f", name, amp);
		view_draw_header(self, str, x, y);
	}

	glScissor(x, y, width, height);
	glViewport(x, y, width, height);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	graph_draw(graph);
	graph_draw_text(graph, self->width, self->height);
}

///pfun
void view_draw_header(View* self, char* str, int x, int y) {
	// Draws a header-tekst above one of the graphs or the boat.
	int width = self->plot_width;
	int height = self->plot_height;

	text_draw_string(self->text, x + width/4, self->height-height-y-10, str);
}

///efun
void view_fields_begin(View* self) {
	for (int i = 0; i < self->field_size; i++) {
		free(self->fields[i]);
	}
	self->field_size = 0;
}

///efun
void view_set_field(View* self, char* title, double value) {
	ViewField* field = (ViewField*) calloc(1, sizeof(ViewField));
	field->title = title;
	field->value = value;
	self->fields[self->field_size] = field;
	self->field_size++;
}

///efun
void view_draw_fields(View* self) {
	int x = self->x2;
	int y = self->y2;
	shaderprogram_use_none();

	// Tegner tid
	char* time = view_get_time_string(self);
	text_draw_string(self->text, x, y, time);

	for (int i = 0; i < self->field_size; i++) {
		char str[100];
		ViewField* field = self->fields[i];
		sprintf(str, "%s: %lf", field->title, field->value);
		text_draw_string(self->text, x, y + (i+1) *20, str);
	}
}

///efun
void view_write_to_file(View* self, char* file_path) {
	FILE* file = fopen(file_path, "w");
	if (file == NULL) {
		printf("Error opening file for writing: %s\n", file_path);
		exit(EXIT_FAILURE);
	}
	int graph_size = self->velocity_graph->history->size;
	for (int i = 0; i < graph_size; i++) {
		double distance = self->distance_graph->history->data[i];
		double velocity = self->velocity_graph->history->data[i];
		double power = self->power_graph->history->data[i];
		fprintf(file, "%10.3f, %10.3f, %10.3f\n", distance, velocity, power);
	}
	fclose(file);
}

///efun
void view_read_from_file(View* self, char* file_path) {
	FILE* file = fopen(file_path, "r");
	if (file == NULL) {
		printf("failure\n");
		printf("Error opening file for reading: %s\n", file_path);
		exit(EXIT_FAILURE);
	}

	char* file_format = "%lf, %lf, %lf\n";
	size_t read;
	size_t len;
	char* line = NULL;
	while ((read = getline(&line, &len, file)) != -1) {
		double power;
		double distance;
		double velocity;

		sscanf(line, file_format, &distance, &velocity, &power);

		graph_push_data(self->velocity_graph, velocity);
		graph_push_data(self->power_graph, power);
		graph_push_data(self->distance_graph, distance);
	}
	fclose(file);
}
