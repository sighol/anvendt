#pragma once

#define GLUT_KEY_ESCAPE 27
#define GLUT_KEY_0 48
#define GLUT_KEY_1 49
#define GLUT_KEY_2 50
#define GLUT_KEY_3 51
#define GLUT_KEY_4 52
#define GLUT_KEY_C 99
#define GLUT_KEY_F 102

///public
void toggle_fullscreen(int width, int height);
void print_fps(void);
///endpublic
