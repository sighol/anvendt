#pragma once

typedef struct {
	double* data;
	int size;
	int allocated;
} History;

///public
History* history_new(int allocated);
void history_destroy(History* self);
void history_push(History* self, double value);
int history_size(History* self);
int history_get(History* self, int size);
void history_clean(History* self);
///endpublic
