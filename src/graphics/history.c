#include <stdlib.h>
#include <stdio.h>

#include "history.h"

///private
void history_print(History* self);
///endprivate

///efun
History* history_new(int allocated) {
	History* self = (History*) malloc(sizeof(History));
	self->allocated = allocated;
	self->size = 0;
	self->data = (double*) calloc(self->allocated, sizeof(double));
	return self;
}

///efun
void history_destroy(History* self) {
	free(self->data);
	free(self);
}

///efun
void history_push(History* self, double value) {
	self->data[self->size % self->allocated] = value;
	self->size++;

	// For performance reasons. Modulo is slower when the input is large.
	// Allocated is multiplied by 2 since some previous history is required for
	// history_size to return the correct value.
	if (self->size > self->allocated * 2) {
		self->size -= self->allocated;
	}
}

///efun
int history_size(History* self) {
	// The size is the smallest of the two
	if (self->size < self->allocated ) {
		return self->size;
	} else {
		return self->allocated;
	}
}

///efun
int history_get(History* self, int size) {
	return self->data[size % self->allocated];
}

///pfun
void history_print(History* self) {
	for (int i = 0; i < self->allocated; i++) {
		printf("%f, ", self->data[i]);
	}
	printf("\n");
}

///efun
void history_clean(History* self) {
	for (int i = 0; i < self->allocated; i++) {
		self->data[i] = 0;
	}
	self->size = 0;
}
