#include "graph.h"

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include "util/util.h"
#include <GL/freeglut.h>
#include <math.h>
#include "graphics/shaderprogram.h"


///private
void graph_init_data(Graph* self);
void graph_compute_min_max(Graph* self, double value);
void graph_update_graph_buffer(Graph* self);
VertexData* graph_get_vertices(Graph* self);
double graph_normalize_y(Graph* self, double in);
double graph_normalize_x(Graph* self, double in);
void graph_push_to_buffer(Graph* self, VertexData* vertices, int vertex_size);
void graph_update_axis_buffer(Graph* self);
void graph_compute_steps(Graph* self);
void graph_print(VertexData* data, int size);
///endprivate

///efun
Graph* graph_new(int history_size) {
	Graph* self = (Graph*) calloc(1, sizeof(Graph));
	self->graph_max = 0;
	self->graph_min = 0;
	self->color_shader_id = 1;
	self->position_shader_id = 0;
	self->text = text_new(GLUT_BITMAP_HELVETICA_12);
	self->axis_steps = (double*) calloc(30, sizeof(double));
	self->history = history_new(history_size);

	self->graph_color = (Color) {255, 0, 0, 255};
	graph_init_data(self);

	return self;
}

///pfun
void graph_init_data(Graph* self) {
	self->graph_buffer = buffer_new();
	self->axis_buffer = buffer_new();
	self->extra_buffer = buffer_new();
}

///efun
void graph_destroy(Graph* self) {
	text_destroy(self->text);
	history_destroy(self->history);
	buffer_destroy(self->graph_buffer);
	buffer_destroy(self->axis_buffer);
	buffer_destroy(self->extra_buffer);

	free(self->axis_steps);
	free(self);
}

///efun
void graph_push_data(Graph* self, double value) {
	// Pushes the value to the history and updates the graph and axis_buffer
	history_push(self->history, value);
	graph_compute_min_max(self, value);
	graph_update_graph_buffer(self);
	graph_update_axis_buffer(self);
}

///pfun
void graph_compute_min_max(Graph* self, double value) {
	if (value > self->graph_max) {
		self->graph_max = value;
	}
	if (value < self->graph_min) {
		self->graph_min = value;
	}
}

///efun
void graph_set_line(Graph* self, double y) {
	buffer_bind(self->extra_buffer);
	Color line_color = {0, 255, 0, 255};
	double line = graph_normalize_y(self, y);
	VertexData lines[2] = {
		{line_color,{-0.88, line, -0.05}},
		{line_color,{0.80, line, -0.05}},
	};
	graph_push_to_buffer(self, lines, sizeof(lines));
}

///pfun
void graph_update_graph_buffer(Graph* self) {
	buffer_bind(self->graph_buffer);
	int size = self->history->size;

	int vertex_size = sizeof(VertexData) * size;

	VertexData* vertices = graph_get_vertices(self);
	graph_push_to_buffer(self, vertices, vertex_size);
	free(vertices);
}

///pfun
VertexData* graph_get_vertices(Graph* self) {
	Color color = self->graph_color;
	VertexData* data = (VertexData*) calloc(self->history->size, sizeof(VertexData));
	int hist_size = history_size(self->history);

	// This is the current index of the history. Everything before this is new
	// data and everything after is old.
	int hist_index = self->history->size % self->history->allocated;

	for (int i = 0; i < hist_size; i++) {
		int index;
		if (i < hist_index) {
			// Data is new => should be put at the end of the graph
			index = hist_size - hist_index + i;
		} else {
			// Data is old => should be put at the beginning of the graph
			index = i - hist_index;
		}
		GLfloat x = graph_normalize_x(self, (double) index);
		GLfloat y = graph_normalize_y(self, self->history->data[i]);
		GLfloat z = -0.1;
		data[index] = (VertexData) {
			color, {x, y, z}
		};
	}
	return data;
}

///pfun
double graph_normalize_y(Graph* self, double in) {
	// Returnes the y-value for use in opengl from the y-value in the history
	return normalize(in, self->graph_min, self->graph_max, -0.9, 0.9);
}

///pfun
double graph_normalize_x(Graph* self, double in) {
	// Returnes the x-value for use in opengl from the x-value in the history
	return normalize(in, 0, history_size(self->history), -0.9, 0.8);
}

///pfun
void graph_push_to_buffer(Graph* self, VertexData* vertices, int vertex_size) {
	glBufferData(GL_ARRAY_BUFFER, vertex_size, &vertices[0], GL_DYNAMIC_DRAW);

	glVertexAttribPointer(
		self->color_shader_id,
		4,
		GL_UNSIGNED_BYTE,
		GL_TRUE,
		sizeof(VertexData), 0);

	glVertexAttribPointer(
		self->position_shader_id,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(VertexData),
		(void*)(sizeof(vertices[0].color)));

	glEnableVertexAttribArray(self->color_shader_id);
	glEnableVertexAttribArray(self->position_shader_id);
}

///pfun
void graph_update_axis_buffer(Graph* self) {
	buffer_bind(self->axis_buffer);

	graph_compute_steps(self);

	Color axis_color = {0, 0, 0, 255};
	Color hint_color = {200,200,200,255};

	// 4 vertices for the x and y axis, and 4 vertices for each of the axis
	// steps
	int vertex_count =  4 + 4 * self->axis_steps_size;
	int vertex_size = vertex_count * sizeof(VertexData);
	VertexData* vertices = (VertexData*) calloc(vertex_count, sizeof(VertexData));

	GLfloat axis_y_value = graph_normalize_y(self, 0.0);
	int axis_start = 4 * self->axis_steps_size;

	// The axis starts at the end of the array
	VertexData* axis = &vertices[axis_start];
	axis[0] = (VertexData){axis_color, {-0.9, -1, 0}};
	axis[1] = (VertexData){axis_color, {-0.9,  1, 0}};
	axis[2] = (VertexData){axis_color, {-0.95,  axis_y_value, 0}};
	axis[3] = (VertexData){axis_color, { 0.80,  axis_y_value, 0}};

	// The steps start at the beginning of the array
	VertexData* steps = &vertices[0];

	for (int i = 0; i < self->axis_steps_size; i++) {
		GLfloat line = graph_normalize_y(self, self->axis_steps[i]);
		// Each step takes 4 positions. Need to jump 4 positions for each
		// axis_step;
		VertexData* step = &steps[i*4];
		step[0] = (VertexData) {axis_color,{-0.92, line, 0}};
		step[1] = (VertexData) {axis_color,{-0.88, line, 0}};
		step[2] = (VertexData) {hint_color, {-0.88, line, 0}};
		step[3] = (VertexData) {hint_color, { 0.80, line, 0}};
	}

	graph_push_to_buffer(self, vertices, vertex_size);
	free(vertices);
}


///pfun
void graph_compute_steps(Graph* self) {
	double min = self->graph_min;
	double max = self->graph_max;

	double diff = max - min;

	// If the graph history is a straight line, we don't know where to place the
	// hint lines
	if (diff == 0.0) {
		self->axis_steps_size = 0;
		return;
	}

	double order = log(diff) / log(10);
	double int_part = (long) order;
	double float_part = order - int_part;
	double step_size = 1;

	// These number are found by trial and error so that the graph helper axises
	// are well placed.
	if (float_part < 0.1) {
		step_size = pow(10.0, int_part) * 0.1;
	} else if (float_part < 0.55) {
		step_size = pow(10.0, int_part) * 0.25;
	} else if (float_part < 0.85) {
		step_size = pow(10.0, int_part) * 0.5;
	} else {
		step_size = pow(10.0, int_part);
	}

	double current_step = 0;
	int i = 0;
	double* data = self->axis_steps;

	// determine steps below zero
	while (current_step >= min) {
		current_step -= step_size;
		data[i] = current_step;
		i++;
	}
	current_step = 0.0;
	// determine steps above zero
	while (current_step <= max) {
		current_step += step_size;
		data[i] = current_step;
		i++;
	}
	self->axis_steps_size = i;
}

///efun
void graph_draw(Graph* self) {
	shaderprogram_use_pass_through();

	buffer_bind(self->axis_buffer);
	glDrawArrays(GL_LINES, 0, 4+ 4 * self->axis_steps_size);

	buffer_bind(self->graph_buffer);
	int hist_size = history_size(self->history);
	glDrawArrays(GL_LINE_STRIP, 0, hist_size);

	buffer_bind(self->extra_buffer);
	glDrawArrays(GL_LINES, 0, 2);
}

///efun
void graph_draw_text(Graph* self, int width, int height) {
	// On some linux graphics drivers, the text isn't drawed if a shader program
	// is present. Therefor the shader program is disabled before text writing.
	shaderprogram_use_none();

	double x_screen_center = (double)width/2.0;
	double x_opengl = 0.8;
	double x_screen = x_screen_center * (1 + x_opengl);

	double y_screen_center = (double)height/2.0;

	for (int i = 0; i < self->axis_steps_size; i++){
		double y_opengl = graph_normalize_y(self, self->axis_steps[i]);
		double y_screen = (1.0 - y_opengl) * y_screen_center;
		char str[15];
		sprintf(str, "%5.2f", self->axis_steps[i]);

		text_draw_string(self->text, (int)x_screen, (int)y_screen, str);
	}
}

///efun
double graph_last_value(Graph* self) {
	int last_index = self->history->size-1;
	return history_get(self->history, last_index);
}

///efun
void graph_clean_history(Graph* self) {
	history_clean(self->history);
}
