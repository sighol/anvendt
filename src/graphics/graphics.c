#include "graph.h"
#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdlib.h>

#include "graphics.h"

///private
void graphics_init_window(int width, int height);
void graphics_init_glew(void);
void graphics_init_gl(void);
///endprivate

///efun
void graphics_init(int width, int height) {
	graphics_init_window(width, height);
	graphics_init_glew();
	graphics_init_gl();
}

///pfun
void graphics_init_window(int width, int height) {
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitContextVersion(3,0);
	glutCreateWindow("Visualisering");
}

///pfun
void graphics_init_glew(void) {
	glewExperimental = GL_TRUE;
	GLenum glewInitResult = glewInit();
	if (GLEW_OK != glewInitResult) {
		printf("%s\n", "Unable to initialize GLEW ... exiting");
		printf("%s\n", glewGetErrorString(glewInitResult));
		exit(EXIT_FAILURE);
	}
}

///pfun
void graphics_init_gl(void) {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
}
