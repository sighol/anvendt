#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "graphics/shaderprogram.h"

///private
Shader* shader_new(GLenum type, const char* code);
void shader_destroy(Shader* self);
void shader_print_error(Shader* self);
///endprivate

static char* vert_shaderprogram_pass = "#version 130\n"
	"\n"
	"in vec4 vPosition;\n"
	"in vec4 vColor;\n"
	"\n"
	"out vec4 exColor;\n"
	"\n"
	"void main() {\n"
	"       gl_Position = vPosition;\n"
	"       exColor = vColor;\n"
	"}\n";

static char* frag_shaderprogram_pass = "#version 130\n"
	"\n"
	"in vec4 exColor;\n"
	"out vec4 fColor;\n"
	"\n"
	"void main() {\n"
	"       fColor = exColor;\n"
	"}\n";


///efun
ShaderProgram* shaderprogram_new (void) {
	ShaderProgram* self = (ShaderProgram*) calloc(1, sizeof(ShaderProgram));
	self->id = 0;
	self->shader_size = 0;
	self->shader_allocated = 2;
	self->shaders = (Shader**) calloc(self->shader_allocated, sizeof(Shader));
	return self;
}

///efun
void shaderprogram_destroy(ShaderProgram* self) {
	for (size_t i = 0; i < self->shader_size; i++) {
		shader_destroy(self->shaders[i]);
	}
	free(self->shaders);
	free(self);
}

///efun
ShaderProgram* shaderprogram_new_pass_through(void) {
	ShaderProgram* self = shaderprogram_new();
	shaderprogram_add(self, vert_shaderprogram_pass, GL_VERTEX_SHADER);
	shaderprogram_add(self, frag_shaderprogram_pass, GL_FRAGMENT_SHADER);
	shaderprogram_compile(self);
	return self;
}

///pfun
Shader* shader_new(GLenum type, const char* code) {
	Shader* self = (Shader*) calloc(1, sizeof(Shader));
	self->type = type;
	self->id = glCreateShader(self->type);
	const char* shaderSources[1];
	shaderSources[0] = code;

	glShaderSource(self->id, 1, (const GLchar**) shaderSources, NULL);
	glCompileShader(self->id);
	shader_print_error(self);
	return self;
}

///pfun
void shader_destroy(Shader* self) {
	free(self);
}

///pfun
void shader_print_error(Shader* self) {
	GLint status;
	glGetShaderiv(self->id, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {
	    GLint infoLogLength;
	    glGetShaderiv(self->id, GL_INFO_LOG_LENGTH, &infoLogLength);

	    GLchar* strInfoLog =  (GLchar*) calloc(infoLogLength, sizeof(GLchar));
	    glGetShaderInfoLog(self->id, infoLogLength, NULL, strInfoLog);

	    fprintf(stderr, "Compilation error in shader(%d): %s\n", self->id, strInfoLog);
	    free(strInfoLog);
	    exit(EXIT_FAILURE);
	}
}

///efun
void shaderprogram_add(ShaderProgram* self, const char* code, GLenum type) {
	Shader* shader = shader_new(type, code);
	if (self->shader_size == self->shader_allocated) {
		self->shader_allocated *= 2;
		self->shaders = realloc(self->shaders,
				self->shader_allocated * sizeof(Shader));

	}
	self->shaders[self->shader_size] = shader;
	self->shader_size++;
}

///efun
void shaderprogram_add_file(ShaderProgram* self, const char* filename, GLenum type) {
	FILE* file = fopen(filename, "r");
	if (file == NULL) {
		fprintf(stderr, "ERROR: could not find shader file: %s\n", filename);
		exit(EXIT_FAILURE);
	}
	fseek(file, 0L, SEEK_END);
	long file_size = ftell(file);
	rewind(file);
	char* buffer = (char*) calloc(file_size + 1, sizeof(char));
	bool read_ok = fread(buffer, file_size, 1, file);
	if (!read_ok) {
		fprintf(stderr, "ERROR: entire read fails: %s\n", filename);
	}
	fclose(file);
	shaderprogram_add(self, buffer, type);

	free(buffer);
}

///efun
void shaderprogram_compile(ShaderProgram* self) {
	self->id = glCreateProgram();
	for (size_t i = 0; i < self->shader_size; i++) {
		glAttachShader(self->id, self->shaders[i]->id);
	}
	glLinkProgram(self->id);
}

///efun
void shaderprogram_use(ShaderProgram* self) {
	if (self->id == 0) {
		fprintf(stderr, "ERROR: ShaderProgram not compiled\n");
		exit(EXIT_FAILURE);
	}
	glUseProgram(self->id);
}

///efun
void shaderprogram_use_pass_through() {
	static ShaderProgram* program;
	if (program == NULL) {
		program = shaderprogram_new_pass_through();
	}
	shaderprogram_use(program);
}

///efun
void shaderprogram_use_none() {
	glUseProgram(0);
}

