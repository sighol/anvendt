#include <stdlib.h>
#include <stdio.h>
#include <graphics/shaderprogram.h>
#include <GL/freeglut.h>
#include "util/util.h"


#include "graphics/boat.h"

///private
void boat_init_boat_data(Boat* self);
void boat_init_setline_data(Boat* self);
void boat_init_axis_data(Boat* self);
double boat_get_axis_step_size(Boat* self);
void boat_buffer_data(Boat* self, size_t size, VertexData* data);
double boat_normalize_x(double x);
///endprivate

///efun
Boat* boat_new(void) {
	Boat* self = calloc(1, sizeof(Boat));

	self->boat_buffer = buffer_new();
	self->setpoint_line_buffer = buffer_new();
	self->axis_buffer = buffer_new();

	self->axis_count = 5;
	self->axis_min = 0;
	self->axis_max = 2000;

	self->text = text_new(GLUT_BITMAP_HELVETICA_12);

	boat_init_boat_data(self);
	boat_init_axis_data(self);
	return self;
}

///efun
void boat_destroy(Boat* self) {
	buffer_destroy(self->boat_buffer);
	buffer_destroy(self->setpoint_line_buffer);
	buffer_destroy(self->axis_buffer);
	text_destroy(self->text);
	free(self);
}

///pfun
void boat_init_boat_data(Boat* self) {
	Color boat_color = {0, 50, 100, 255};

	// Initiating boat.
	VertexData nodes[] = {
		{boat_color, { 0,  0, 0}}, {boat_color, { 3, -4, 0}},
		{boat_color, {15, -4, 0}}, {boat_color, {15, -3, 0}},
		{boat_color, {16, -3, 0}}, {boat_color, {16,  0, 0}},
		{boat_color, {13,  0, 0}}, {boat_color, {13,  4, 0}},
		{boat_color, { 9,  4, 0}}, {boat_color, { 8,  0, 0}},
	};

	VertexData boat[] = {
		nodes[9], nodes[0], nodes[1],
		nodes[2], nodes[3], nodes[4],
		nodes[5], nodes[6], nodes[7],
		nodes[8],
	};

	double boat_size_nodes = 16;
	double boat_size_gl = 0.5;
	double boat_scale = boat_size_gl / boat_size_nodes;

	// Scales and positions the boat into place
	double x_position = boat_normalize_x(self->position);
	for (int i = 0; i < sizeof(boat)/sizeof(VertexData); i++) {
		boat[i].position.x = boat[i].position.x *  boat_scale + x_position;
		boat[i].position.y = boat[i].position.y *  boat_scale;
	}

	buffer_bind(self->boat_buffer);
	boat_buffer_data(self, sizeof(boat), boat);
}

///pfun
void boat_init_setline_data(Boat* self) {
	Color line_color = {0, 255, 0, 255};
	double line_x = boat_normalize_x(self->setpoint);
	VertexData line[] = {
		{line_color, {line_x, -1, 0}},
		{line_color, {line_x,  1, 0}},
	};

	buffer_bind(self->setpoint_line_buffer);
	boat_buffer_data(self, sizeof(line), line);
}

///pfun
void boat_init_axis_data(Boat* self) {
	double axis_step_size = boat_get_axis_step_size(self);

	VertexData axis[self->axis_count*2];
	Color axis_color = {255, 255, 255, 255};

	double step = self->axis_min;
	for (int i = 0; i < self->axis_count; i++) {
		double x = boat_normalize_x(step);
		axis[i*2] = (VertexData) {axis_color, {x, -1, 0}};
		axis[i*2+1] = (VertexData) {axis_color, {x, 1, 0}};
		step += axis_step_size;
	}

	buffer_bind(self->axis_buffer);
	boat_buffer_data(self, sizeof(axis), axis);
}

///pfun
double boat_get_axis_step_size(Boat* self) {
	return (self->axis_max - self->axis_min) / (self->axis_count-1);
}

///pfun
void boat_buffer_data(Boat* self, size_t size, VertexData* data) {
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*) sizeof(Color));
	glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), 0);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
}

///pfun
double boat_normalize_x(double x) {
	return normalize(x, 0, 2500, 0.3, -1);
}

///efun
void boat_draw(Boat* self, double setpoint, double position) {
	self->setpoint = setpoint;
	self->position = position;
	shaderprogram_use_pass_through();

	boat_init_boat_data(self);
	boat_init_setline_data(self);

	buffer_bind(self->boat_buffer);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 10);

	buffer_bind(self->setpoint_line_buffer);
	glDrawArrays(GL_LINES, 0, 2);

	buffer_bind(self->axis_buffer);
	glDrawArrays(GL_LINES, 0, self->axis_count*2);
}

///efun
void boat_draw_text(Boat* self, int width, int height) {
	double axis_step_size = boat_get_axis_step_size(self);
	double step = self->axis_min;


	for (int i = 0; i < self->axis_count; i++) {
		double y = height - height/7;

		double x_gl = boat_normalize_x(step);
		double x =  (x_gl + 1) * width/2;

		char str[15];
		sprintf(str, "%5.0f", step);
		text_draw_string(self->text, (int)x, (int)y, str);
		step += axis_step_size;
	}

}
