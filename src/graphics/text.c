#include "text.h"

#include <stdlib.h>
#include <GL/freeglut.h>
#include <string.h>


///private
///endprivate

///efun
TextWriter* text_new(void* bitmap) {
	TextWriter* self = (TextWriter*) calloc(1, sizeof(TextWriter));
	self->glyphs_display_list = glGenLists(256);
	self->color = (TextColor){255,255,255};

	for (int i = 0; i < 256; i++) {
		glNewList(self->glyphs_display_list + i, GL_COMPILE);
		glutBitmapCharacter(bitmap, i);
		glEndList();
	}
	return self;
}

///efun
void text_destroy(TextWriter* self) {
	free(self);
}

///efun
void text_draw_string(TextWriter* self, int x, int y, const char * text) {
	TextColor* c = &self->color;
	glColor3ub(c->r, c->g, c->b);
	glListBase(self->glyphs_display_list);

	// Centering the text
	glRasterPos2i(x+1, y + 4);
	glCallLists((GLsizei)strlen(text), GL_UNSIGNED_BYTE, text);
}
