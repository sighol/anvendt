#pragma once

#include <GL/glew.h>


typedef struct {
	GLuint id;
	GLenum type;
} Shader;

typedef struct {
	GLuint id;
	Shader** shaders;
	size_t shader_size;
	size_t shader_allocated;
} ShaderProgram;

///public
ShaderProgram* shaderprogram_new (void);
void shaderprogram_destroy(ShaderProgram* self);
ShaderProgram* shaderprogram_new_pass_through(void);
void shaderprogram_add(ShaderProgram* self, const char* code, GLenum type);
void shaderprogram_add_file(ShaderProgram* self, const char* filename, GLenum type);
void shaderprogram_compile(ShaderProgram* self);
void shaderprogram_use(ShaderProgram* self);
void shaderprogram_use_pass_through();
void shaderprogram_use_none();
///endpublic
