#include <stdlib.h>
#include <stdio.h>

#include "graphics/buffer.h"

///private
///endprivate

///efun
Buffer* buffer_new(void) {
	Buffer* self = calloc(1, sizeof(Buffer));
	glGenVertexArrays(1, &self->vao_id);
	glGenBuffers(1, &self->buffer_id);
	return self;
}

///efun
void buffer_destroy(Buffer* self) {
	glDeleteVertexArrays(1, &self->vao_id);
	glDeleteBuffers(1, &self->buffer_id);
	free(self);
}

///efun
void buffer_bind(Buffer* self) {
	glBindVertexArray(self->vao_id);
	glBindBuffer(GL_ARRAY_BUFFER, self->buffer_id);
}


///efun
void buffer_print_vertex_data(size_t size, VertexData* data) {
	for (size_t i = 0; i < size; i++) {
		Color* c = &data[i].color;
		Position* p = &data[i].position;
		printf("Color: {%d, %d, %d}, ", c->r, c->g, c->b);
		printf("Position: {%f, %f, %f}\n", p->x, p->y, p->z);
	}
}
