#pragma once

#include "graphics/text.h"
#include "graphics/graph.h"
#include "graphics/boat.h"

enum GRAPHS {GRAPH_VELOCITY, GRAPH_DISTANCE, GRAPH_POWER, GRAPH_SIZE};

typedef struct {
	char* title;
	double value;
} ViewField;

typedef struct {
	int height;
	int width;

	int plot_width, plot_height;
	int x1, x2, y1, y2;
	Graph* velocity_graph;
	Graph* distance_graph;
	Graph* power_graph;
	Boat* boat;
	TextWriter* text;

	ViewField** fields;
	int field_size;

	double setpoint;
	double position;

} View;


///public
View* view_new(void);
void view_destroy(View* self);
void view_reshape(View* self, int width, int height);
void view_clean_history(View* self);
void view_draw(View* self);
void view_fields_begin(View* self);
void view_set_field(View* self, char* title, double value);
void view_draw_fields(View* self);
void view_write_to_file(View* self, char* file_path);
void view_read_from_file(View* self, char* file_path);
///endpublic
