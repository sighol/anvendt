#pragma once

#include <graphics/buffer.h>
#include "graphics/text.h"

typedef struct {
	double position;
	double setpoint;
	Buffer* boat_buffer;
	Buffer* setpoint_line_buffer;
	Buffer* axis_buffer;
	int axis_count;
	int axis_min;
	int axis_max;
	TextWriter* text;
} Boat;

///public
Boat* boat_new(void);
void boat_destroy(Boat* self);
void boat_draw(Boat* self, double setpoint, double position);
void boat_draw_text(Boat* self, int width, int height);
///endpublic
