#include "simulate_main.h"

///private
///endprivate

Main* main_global;

///efun
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	main_global = main_new();
	glutCloseFunc(close_callback);
	glutDisplayFunc(display_callback);
	glutIdleFunc(idle_callback);
	glutReshapeFunc(reshape_callback);
	glutKeyboardFunc(keyboard_callback);

	glutMainLoop();
};

///efun
Main* main_new(void) {
	Main* self = (Main*) malloc(sizeof(Main));
	self->width = 800;
	self->height = 600;
	graphics_init(self->width, self->height);
	self->derivator = derivator_new(10);
	self->next = 0;
	self->view = view_new();
	self->setpoint = 500;
	self->graph = self->view->velocity_graph;
	self->elapsed_time = 0;

	return self;
}

void main_destroy(Main* self) {
	derivator_destroy(self->derivator);
	view_destroy(self->view);
	free(self);
}

///efun
void keyboard_callback(unsigned char key, int x, int y) {
	Main* self = main_global;
    if (key == 27) {
        exit(0);
    }
    if (key == GLUT_KEY_C) {
    	view_clean_history(main_global->view);
    }

    if (key == GLUT_KEY_F) {
    	toggle_fullscreen(self->width, self->height);
    }

    if (key == GLUT_KEY_0) {
    	self->setpoint = 0;
    } else if(key == GLUT_KEY_1) {
    	self->setpoint = 500;
    } else if (key == GLUT_KEY_2) {
    	self->setpoint = 1000;
    } else if (key == GLUT_KEY_3) {
    	self->setpoint = 1500;
    } else if (key == GLUT_KEY_4) {
    	self->setpoint = 2000;
    }

}

///efun
void reshape_callback(int width, int height) {
	Main* self = main_global;
	view_reshape(self->view, width, height);
    glViewport(0, 0, width, height);
    glScissor(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, /*near=*/0, /*far=*/1);

    glutPostRedisplay();
}

///efun
void idle_callback(void) {
	glutPostRedisplay();
}

///efun
void display_callback(void) {

	Main* self = main_global;
	print_fps();
	simulate_controller(self);

	view_fields_begin(self->view);
	view_set_field(self->view, "Neste", self->next);
	view_set_field(self->view, "Power", self->power);
	view_set_field(self->view, "Power_p", self->power_p);
	view_set_field(self->view, "Power_i", self->power_i);
	view_set_field(self->view, "Power_d", self->power_d);

	self->view->position = self->next;
	self->view->setpoint = self->setpoint;

	view_draw(self->view);

	glutSwapBuffers();
	glFlush();
}

///efun
void simulate_controller(Main* self) {
	static double integrator = 0;
	static double velocity = 0;
	static double pos = 0;

	double diff = self->setpoint - pos;
	derivator_push(self->derivator, pos);
	derivator_speed(self->derivator);

	double Kp = 4.8;
	double Ki = 0.4;
	double Kd = 20	;

	if (diff > 100/Kp) {
		integrator += 100/Kp;
	} else {
		integrator += diff;
	}

	if (integrator < 0) {
		integrator = 0;
	}

	double power_p = Kp * diff;
	double power_i = Ki * integrator;
	double power_d = Kd * velocity;

	double power = power_p + power_i - power_d;

	if (power > 800) {
		power = 800;
	}
	if (power < -100) {
		power = -100;
	}



	graph_push_data(self->view->velocity_graph, velocity);
	graph_push_data(self->view->power_graph, 	power);
	graph_push_data(self->view->distance_graph, pos);

	simulate_apply_power(self, &pos, &velocity, power);

	self->power_p = power_p;
	self->power_i = power_i;
	self->power_d = power_d;
	self->power = power;
}
///efun
void simulate_apply_power(Main* self, double* pos, double* vel, double power) {
	static const double M = 300.3;
	static const double B11 = 40;
	static const double line_drag = 400;


	double A11 = M;
	double MA = A11 + M;

	Matrix* A = matrix_new_input(2, 2, (double[4]) {
		0, 1,
		0, -B11/MA,
	});


	Matrix* B = matrix_new_input(2, 1, (double[2]) {
		0,
		1.0/MA,
	});


	Matrix* x = matrix_new_input(2, 1, (double[2]){
		*pos,
		*vel,
	});

	Matrix* AX = matrix_multiply(A, x);
	Matrix* BU = matrix_multiply_scalar(B, power);

	Matrix* BW = matrix_multiply_scalar(B, line_drag);

	Matrix* AX_BU = matrix_add(AX, BU);
	Matrix* x_diff = matrix_sub(AX_BU, BW);

	Matrix* x_new = matrix_add(x, x_diff);

	*pos = matrix_get(x_new, 0, 0);
	*vel = matrix_get(x_new, 1, 0);
	self->next = *pos;

	matrix_destroy(A);
	matrix_destroy(B);
	matrix_destroy(AX);
	matrix_destroy(BU);
	matrix_destroy(BW);
	matrix_destroy(AX_BU);
	matrix_destroy(x_new);
}

///efun
void close_callback(void) {
	main_destroy(main_global);
}
